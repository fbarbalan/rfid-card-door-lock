#include "pico.h"
#include "pico/stdlib.h"
#include <stdio.h>
#include "hardware/pwm.h"
#include "hardware/gpio.h"
#include "pico/binary_info.h"

const uint LED_PIN = 25;
const uint BUTTON_PIN = 28;
float wrap = 39062;
float clockDiv = 64;

bool direction = true;
int currentMillis = 400;
int servoPin = 22;

void setMillis(int servoPin, float millis)
{
    pwm_set_gpio_level(servoPin, (millis / 20000.f) * 39062.f);
}

void setServo(int servoPin, float startMillis)
{
    gpio_set_function(servoPin, GPIO_FUNC_PWM);
    uint slice_num = pwm_gpio_to_slice_num(servoPin);

    pwm_config config = pwm_get_default_config();
    pwm_config_set_clkdiv(&config, 64.f);
    pwm_config_set_wrap(&config, 39062.f);

    pwm_init(slice_num, &config, true);

    setMillis(servoPin, startMillis);
}

int main()
{
    stdio_init_all();
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);
    gpio_put(LED_PIN, 0);

    gpio_init(BUTTON_PIN);
    gpio_set_dir(BUTTON_PIN, GPIO_IN);
    gpio_pull_up(BUTTON_PIN);

    setServo(servoPin, currentMillis);

    int aux = 0; //so that the sleep timer won't work in the first iteration of the while loop, thus not delaying the motor from turning on button push

    while (true)
    {
        if (!gpio_get(BUTTON_PIN)) // button is pushed
        {
            currentMillis = 2400;
            gpio_put(LED_PIN, 1);
            aux = 1;
        }
        if (gpio_get(BUTTON_PIN)) // button is not pushed
        {
            currentMillis = 400;
            if (aux == 1)
            {
                sleep_ms(5000);
                gpio_put(LED_PIN, 0);
                aux = 0;
            }
        }
        setMillis(servoPin, currentMillis);
    }
}